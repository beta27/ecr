FROM nginx:alpine
COPY ./* ./html/

#PROD ->ARG DT_API_URL="https://10.231.8.254:9999/e/pzw14808/api"

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
